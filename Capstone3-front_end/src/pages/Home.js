import Banner from '../components/Banner';
import CarouselSec from "../components/Carousel";
import Highlights from '../components/Highlights';

import { Container } from 'react-bootstrap';
import { Fragment } from 'react';

const image = new URL("https://www.pexels.com/photo/person-wearing-brown-nike-high-top-sneakers-4061395/")



export default function Home() {
	const data = {
        title: "FLIGHT CLUB SHOES  ",
        content: "AIR JORDAN 13 RETRO 'FRENCH BLUE",
        destination: "/products",
        label: "Shop Now!",
        // image: "../images/background.jpg"
    }

    return (
    
        <Fragment id = " Home-Section">
            <div>
                <img src={image} alt="" />
            </div>
	        <Banner data={data}/>
            <Highlights/>

             <Container>
                <CarouselSec/>
            </Container>

		</Fragment>



    )
}



































// import Banner from '../components/Banner';
// import CarouselSec from "../components/Carousel";
// import Highlights from '../components/Highlights';

// import { Container } from 'react-bootstrap';
// import { Fragment } from 'react';



// export default function Home() {
// 	const data = {
//         title: "Julian Apparel",
//         content: "The best way to shop online! Best and powerful platform for online shopping with better and faster shipping.",
//         destination: "/products",
//         label: "Buy Now!"
//     }

//     return (

//         <Fragment>  
// 	        <Banner data={data}/>
//             <Highlights/>
//              <Container>
//                 <CarouselSec/>
//             </Container>

// 		</Fragment>



//     )
// }
