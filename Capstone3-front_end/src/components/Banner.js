import {Row, Col, Button, Card, Container} from "react-bootstrap";
import {Link} from "react-router-dom";

// import background from './public/background.jpeg';


export default function Banner({data}){


	const {title, content, destination, label} = data;

	return( 	
				<>
			
				<Container  id = "container">
					<Row>
						<Col sm={8}  className="pb-3 pt-3 text">
							<div id = "intro">
							<h1>{title}</h1>
							<p>{content}</p>
							<Button as={Link} to={destination} variant="primary">{label}</Button>
							
							</div>	
							
						
						</Col>
						<Col sm={4}>
						<Card.Img className="pb-3 pt-3" id = "pic-1" src="/images/mainSection.jpeg" />

						</Col>
					</Row>
						</Container>
						</>
				);

  }




























// import {Row, Col, Button, Card} from "react-bootstrap";
// import {Link} from "react-router-dom";

// export default function Banner({data}){


// 	const {title, content, destination, label} = data;

// 	return(
		
// 				<Row  className="pb-5 pt-2">
// 				<Card className="cardBanner">
// 					<Col lg="9" xl="12" className="pb-3 pt-3 text-center">
// 						<h1>{title}</h1>
// 						<p>{content}</p>
// 						<Button as={Link} to={destination} variant="primary">{label}</Button>
// 					</Col>
// 					</Card>
// 				</Row>

		
// 	)
// }
