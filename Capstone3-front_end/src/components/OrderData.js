import { useState, useEffect } from "react";

export default function OrderData({userOrders}) {



    // const [orderId, setOrderId] = useState(userOrders._id);
    // const [userId, setUserId] = useState(userOrders.userId);

    const [orders, setOrders] = useState([]);

    
    const fetchData = () => {
        fetch(`http://localhost:4000/orders/getUserOrder`,
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
                }
        })
        .then(res => res.json())
        .then(data => {setOrders() 
        })
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <>
            {orders}
        </>
    )

}
