import {Row, Col, Card} from 'react-bootstrap';
// import CardGroup from 'react-bootstrap/CardGroup';


export default function Highlights(){
	return(

			<>
				<div id = "highlights">
					<h2 id ="text" >New Releases</h2>
	
				<br />
			<Row xs={1} md={4} className="g-4 pb-5">
	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="/images/p-1.jpeg" />
	            <Card.Body>
	              <Card.Title>AIR JORDAN 13 RETRO 'FRENCH BLUE'</Card.Title>
	              <Card.Text>
				  Inspired by MJ's 'Black Cat' nickname, the Air Jordan 13 features design language lifted from a panther.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/p-2.jpeg" />
	            <Card.Body>
	              <Card.Title>AIR JORDAN 7 RETRO 'CITRUS' 2022</Card.Title>
	              <Card.Text>
				  Reviving an untraditional colorway that was initially released in 2006 is the 2022 edition of the Air Jordan 7 Retro 'Citrus.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/p-6.jpeg" />
	            <Card.Body>
	              <Card.Title>WMNS DUNK LOW 'ROSE WHISPER'</Card.Title>
	              <Card.Text> The Wmns Dunk Low 'Rose Whisper' refreshes the classic style of the retro basketball shoe in a light-hearted hu
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/p-4.jpeg" />
	            <Card.Body>
	              <Card.Title>DUNK LOW 'BLACK WHITE'</Card.Title>
	              <Card.Text> The Dunk Low 'Black White' also known as 'Panda' brings a classic two-tone look to its classic basketball construction. 
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>
		
	    	</Row>	
		</div>
		</>
	  );
	}


































// import {Row, Col, Card} from 'react-bootstrap';


// export default function Highlights(){
// 	return(

// 			<Row xs={1} md={3} className="g-4 pb-5">

// 	        <Col>
// 	          <Card className="cardHighlight">
// 	            <Card.Img variant="top" src="./images/high_quality.jpg" />
// 	            <Card.Body>
// 	              <Card.Title><b>High Quality</b></Card.Title><br/>
// 	              <Card.Text>
// 	               Natural fibers such as cotton, wool, silk, linen, and cashmere are generally a better bet when choosing clothes that will last. While synthetic fibres can bring levels of stability and versatility to garments, as a rule of thumb don't buy a garment with more than a 20% portion of synthetic fiber.
// 	              </Card.Text>
// 	            </Card.Body>
// 	          </Card>
// 	        </Col>

// 	        <Col>
// 	          <Card className="cardHighlight">
// 	            <Card.Img variant="top" src="./images/fast_shipping.jpg" />
// 	            <Card.Body>
// 	              <Card.Title><b>Fast Shipping</b></Card.Title><br/>
// 	              <Card.Text>
// 	                On-demand pick-up and delivery services your business can rely on. Get special quote today. Multi drop-off points. AI-optimized routes. Real-time tracking & photo-proof of delivery. Free Package Protection. Designated Drivers. AI Route Optimization.
// 	              </Card.Text>
// 	            </Card.Body>
// 	          </Card>
// 	        </Col>

// 	        <Col>
// 	          <Card className="cardHighlight">
// 	            <Card.Img variant="top" src="./images/affordable_price.jpg" />
// 	            <Card.Body>
// 	              <Card.Title><b>Affordable Prices</b></Card.Title><br/>
// 	              <Card.Text> We want a good quality railway that is widely available at an affordable price. If other manufacturers wanted to compete by charging less than a particular price, we could ensure a supply of products—including branded products—at an affordable price.
// 	              </Card.Text>
// 	            </Card.Body>
// 	          </Card>
// 	        </Col>   
// 	    </Row>
// 	  );
// 	}